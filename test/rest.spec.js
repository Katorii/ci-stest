const chai = require("chai");
const expect = chai.expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

let server;

// Async request
const arequest = async(value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if (error) reject(error)
        else resolve(response)
    }); 
});

describe("Test REST API", () => {
    beforeEach("Start server", async () => {
        server = app.listen(port);
    });
    describe("Test functionality", () => {
        it("GET /add=a=1&b=2 returns 3", async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/add',
                qs: {a: '1', b: '2'},
            };
            await arequest(options).then((res) => {
                console.log({message: res.body});
                expect(res.body).to.equal("3");
            }).catch((res) => {
                console.log({res});
                expect(true).to.equal(false, "add function failed");
            })
        });
    });

    // Check if any test fails and close the server if so...
    afterEach(() => {
        server.close();
    });
});